
package productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Reviews implements Parcelable {
    @SerializedName("rating_average")
    @Expose
    private double ratingAverage;
    @SerializedName("total")
    @Expose
    private int total;
    public final static Parcelable.Creator<Reviews> CREATOR = new Creator<Reviews>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Reviews createFromParcel(Parcel in) {
            return new Reviews(in);
        }

        public Reviews[] newArray(int size) {
            return (new Reviews[size]);
        }

    };

    protected Reviews(Parcel in) {
        this.ratingAverage = ((double) in.readValue((double.class.getClassLoader())));
        this.total = ((int) in.readValue((int.class.getClassLoader())));
    }

    public Reviews() {
    }

    public double getRatingAverage() {
        return ratingAverage;
    }

    public void setRatingAverage(double ratingAverage) {
        this.ratingAverage = ratingAverage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ratingAverage);
        dest.writeValue(total);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Reviews{");
        sb.append("ratingAverage=").append(ratingAverage);
        sb.append(", total=").append(total);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reviews)) return false;
        Reviews reviews = (Reviews) o;
        return Double.compare(reviews.ratingAverage, ratingAverage) == 0 &&
                total == reviews.total;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ratingAverage, total);
    }
}
