package productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Picture implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("secure_url")
    @Expose
    private String secureUrl;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("max_size")
    @Expose
    private String maxSize;
    @SerializedName("quality")
    @Expose
    private String quality;
    public final static Parcelable.Creator<Picture> CREATOR = new Creator<Picture>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Picture createFromParcel(Parcel in) {
            return new Picture(in);
        }

        public Picture[] newArray(int size) {
            return (new Picture[size]);
        }

    };

    protected Picture(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.url = ((String) in.readValue((String.class.getClassLoader())));
        this.secureUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.size = ((String) in.readValue((String.class.getClassLoader())));
        this.maxSize = ((String) in.readValue((String.class.getClassLoader())));
        this.quality = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Picture() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSecureUrl() {
        return secureUrl;
    }

    public void setSecureUrl(String secureUrl) {
        this.secureUrl = secureUrl;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(String maxSize) {
        this.maxSize = maxSize;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(url);
        dest.writeValue(secureUrl);
        dest.writeValue(size);
        dest.writeValue(maxSize);
        dest.writeValue(quality);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Picture{");
        sb.append("id='").append(id).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", secureUrl='").append(secureUrl).append('\'');
        sb.append(", size='").append(size).append('\'');
        sb.append(", maxSize='").append(maxSize).append('\'');
        sb.append(", quality='").append(quality).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Picture)) return false;
        Picture picture = (Picture) o;
        return Objects.equals(id, picture.id) &&
                Objects.equals(url, picture.url) &&
                Objects.equals(secureUrl, picture.secureUrl) &&
                Objects.equals(size, picture.size) &&
                Objects.equals(maxSize, picture.maxSize) &&
                Objects.equals(quality, picture.quality);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, url, secureUrl, size, maxSize, quality);
    }
}