package productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class ItemDetails implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("stop_time")
    @Expose
    private String stopTime;
    @SerializedName("condition")
    @Expose
    private String condition;
    @SerializedName("pictures")
    @Expose
    private List<Picture> pictures = null;
    @SerializedName("accepts_mercadopago")
    @Expose
    private boolean acceptsMercadopago;
    public final static Parcelable.Creator<ItemDetails> CREATOR = new Creator<ItemDetails>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ItemDetails createFromParcel(Parcel in) {
            return new ItemDetails(in);
        }

        public ItemDetails[] newArray(int size) {
            return (new ItemDetails[size]);
        }

    };

    protected ItemDetails(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.startTime = ((String) in.readValue((String.class.getClassLoader())));
        this.stopTime = ((String) in.readValue((String.class.getClassLoader())));
        this.condition = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.pictures, Picture.class.getClassLoader());
        this.acceptsMercadopago = ((boolean) in.readValue((boolean.class.getClassLoader())));
    }

    public ItemDetails() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    public boolean isAcceptsMercadopago() {
        return acceptsMercadopago;
    }

    public void setAcceptsMercadopago(boolean acceptsMercadopago) {
        this.acceptsMercadopago = acceptsMercadopago;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(startTime);
        dest.writeValue(stopTime);
        dest.writeValue(condition);
        dest.writeList(pictures);
        dest.writeValue(acceptsMercadopago);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ItemDetails{");
        sb.append("id='").append(id).append('\'');
        sb.append(", startTime='").append(startTime).append('\'');
        sb.append(", stopTime='").append(stopTime).append('\'');
        sb.append(", condition='").append(condition).append('\'');
        sb.append(", pictures=").append(pictures);
        sb.append(", acceptsMercadopago=").append(acceptsMercadopago);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemDetails)) return false;
        ItemDetails that = (ItemDetails) o;
        return acceptsMercadopago == that.acceptsMercadopago &&
                Objects.equals(id, that.id) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(stopTime, that.stopTime) &&
                Objects.equals(condition, that.condition) &&
                Objects.equals(pictures, that.pictures);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, startTime, stopTime, condition, pictures, acceptsMercadopago);
    }
}
