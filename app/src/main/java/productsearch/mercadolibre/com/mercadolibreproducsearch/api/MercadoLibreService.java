package productsearch.mercadolibre.com.mercadolibreproducsearch.api;

import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.ItemSearchResults;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.detail.ItemDetails;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MercadoLibreService {
    @GET("sites/{site}/search?")
    Call<ItemSearchResults> listItems(@Path("site") String site, @Query("q") String searchQuery);

    @GET("items/{itemId}")
    Call<ItemDetails> getItemDetails(@Path("itemId") String itemId);
}
