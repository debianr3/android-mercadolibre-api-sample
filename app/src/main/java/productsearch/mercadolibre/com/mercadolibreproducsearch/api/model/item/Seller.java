
package productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class Seller implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("power_seller_status")
    @Expose
    private String powerSellerStatus;
    @SerializedName("car_dealer")
    @Expose
    private Boolean carDealer;
    @SerializedName("real_estate_agency")
    @Expose
    private Boolean realEstateAgency;
    @SerializedName("tags")
    @Expose
    private List<Object> tags = null;
    public final static Creator<Seller> CREATOR = new Creator<Seller>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Seller createFromParcel(Parcel in) {
            return new Seller(in);
        }

        public Seller[] newArray(int size) {
            return (new Seller[size]);
        }

    };

    protected Seller(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.powerSellerStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.carDealer = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.realEstateAgency = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        in.readList(this.tags, (Object.class.getClassLoader()));
    }

    public Seller() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPowerSellerStatus() {
        return powerSellerStatus;
    }

    public void setPowerSellerStatus(String powerSellerStatus) {
        this.powerSellerStatus = powerSellerStatus;
    }

    public Boolean getCarDealer() {
        return carDealer;
    }

    public void setCarDealer(Boolean carDealer) {
        this.carDealer = carDealer;
    }

    public Boolean getRealEstateAgency() {
        return realEstateAgency;
    }

    public void setRealEstateAgency(Boolean realEstateAgency) {
        this.realEstateAgency = realEstateAgency;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Seller{");
        sb.append("id=").append(id);
        sb.append(", powerSellerStatus='").append(powerSellerStatus).append('\'');
        sb.append(", carDealer=").append(carDealer);
        sb.append(", realEstateAgency=").append(realEstateAgency);
        sb.append(", tags=").append(tags);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Seller)) return false;
        Seller seller = (Seller) o;
        return Objects.equals(id, seller.id) &&
                Objects.equals(powerSellerStatus, seller.powerSellerStatus) &&
                Objects.equals(carDealer, seller.carDealer) &&
                Objects.equals(realEstateAgency, seller.realEstateAgency) &&
                Objects.equals(tags, seller.tags);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, powerSellerStatus, carDealer, realEstateAgency, tags);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(powerSellerStatus);
        dest.writeValue(carDealer);
        dest.writeValue(realEstateAgency);
        dest.writeList(tags);
    }

    public int describeContents() {
        return 0;
    }

}
