package productsearch.mercadolibre.com.mercadolibreproducsearch.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public enum ApiClient {
    INSTANCE;

    final MercadoLibreService mService;

    ApiClient() {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.mercadolibre.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mService = retrofit.create(MercadoLibreService.class);
    }

    public MercadoLibreService getService() {
        return mService;
    }
}
