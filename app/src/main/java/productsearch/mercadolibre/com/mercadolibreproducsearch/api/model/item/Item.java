
package productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Item implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("seller")
    @Expose
    private Seller seller;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("currency_id")
    @Expose
    private String currencyId;
    @SerializedName("available_quantity")
    @Expose
    private Integer availableQuantity;
    @SerializedName("sold_quantity")
    @Expose
    private Integer soldQuantity;
    @SerializedName("buying_mode")
    @Expose
    private String condition;
    @Expose
    private String thumbnail;
    @SerializedName("accepts_mercadopago")
    @Expose
    private Boolean acceptsMercadopago;
    @SerializedName("shipping")
    @Expose
    private Shipping shipping;
    @SerializedName("reviews")
    @Expose
    private Reviews reviews;
    public final static Creator<Item> CREATOR = new Creator<Item>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        public Item[] newArray(int size) {
            return (new Item[size]);
        }

    };

    protected Item(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.seller = ((Seller) in.readValue((Seller.class.getClassLoader())));
        this.price = ((Double) in.readValue((Double.class.getClassLoader())));
        this.currencyId = ((String) in.readValue((String.class.getClassLoader())));
        this.availableQuantity = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.soldQuantity = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.condition = ((String) in.readValue((String.class.getClassLoader())));
        this.thumbnail = ((String) in.readValue((String.class.getClassLoader())));
        this.acceptsMercadopago = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.shipping = ((Shipping) in.readValue((Shipping.class.getClassLoader())));
        this.reviews = ((Reviews) in.readValue((Reviews.class.getClassLoader())));
    }

    public Item() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Integer getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(Integer availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public Integer getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(Integer soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Boolean getAcceptsMercadopago() {
        return acceptsMercadopago;
    }

    public void setAcceptsMercadopago(Boolean acceptsMercadopago) {
        this.acceptsMercadopago = acceptsMercadopago;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public Reviews getReviews() {
        return reviews;
    }

    public void setReviews(Reviews reviews) {
        this.reviews = reviews;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(seller);
        dest.writeValue(price);
        dest.writeValue(currencyId);
        dest.writeValue(availableQuantity);
        dest.writeValue(soldQuantity);
        dest.writeValue(condition);
        dest.writeValue(thumbnail);
        dest.writeValue(acceptsMercadopago);
        dest.writeValue(shipping);
        dest.writeValue(reviews);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Item{");
        sb.append("id='").append(id).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", seller=").append(seller);
        sb.append(", price=").append(price);
        sb.append(", currencyId='").append(currencyId).append('\'');
        sb.append(", availableQuantity=").append(availableQuantity);
        sb.append(", soldQuantity=").append(soldQuantity);
        sb.append(", condition='").append(condition).append('\'');
        sb.append(", thumbnail='").append(thumbnail).append('\'');
        sb.append(", acceptsMercadopago=").append(acceptsMercadopago);
        sb.append(", shipping=").append(shipping);
        sb.append(", reviews=").append(reviews);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return Objects.equals(id, item.id) &&
                Objects.equals(title, item.title) &&
                Objects.equals(seller, item.seller) &&
                Objects.equals(price, item.price) &&
                Objects.equals(currencyId, item.currencyId) &&
                Objects.equals(availableQuantity, item.availableQuantity) &&
                Objects.equals(soldQuantity, item.soldQuantity) &&
                Objects.equals(condition, item.condition) &&
                Objects.equals(thumbnail, item.thumbnail) &&
                Objects.equals(acceptsMercadopago, item.acceptsMercadopago) &&
                Objects.equals(shipping, item.shipping) &&
                Objects.equals(reviews, item.reviews);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, title, seller, price, currencyId, availableQuantity, soldQuantity,
                            condition, thumbnail, acceptsMercadopago, shipping, reviews);
    }
}
