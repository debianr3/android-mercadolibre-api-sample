package productsearch.mercadolibre.com.mercadolibreproducsearch.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import productsearch.mercadolibre.com.mercadolibreproducsearch.R;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.ApiClient;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.ItemSearchResults;
import productsearch.mercadolibre.com.mercadolibreproducsearch.data.ItemsListAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = SearchActivity.class.getSimpleName();

    private ItemsListAdapter mItemsListAdapter;

    private RecyclerView mRecyclerView;
    private View mMessageView;
    private TextView mMessageText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        final Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        mRecyclerView = findViewById(R.id.itemsList);
        mRecyclerView.setHasFixedSize(true);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        final DividerItemDecoration itemDecor = new DividerItemDecoration(this,
                                                                          layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(itemDecor);

        mItemsListAdapter = new ItemsListAdapter();
        mRecyclerView.setAdapter(mItemsListAdapter);

        mMessageView = findViewById(R.id.messageView);
        mMessageText = findViewById(R.id.messageText);
    }

    private void requestItems(final String searchQuery) {
        final Call<ItemSearchResults> itemsRequest =
                ApiClient.INSTANCE.getService().listItems("MLA", searchQuery);

        itemsRequest.enqueue(new Callback<ItemSearchResults>() {
            @Override
            public void onResponse(Call<ItemSearchResults> call, Response<ItemSearchResults> response) {
                final ItemSearchResults results = response.body();
                if (results != null && results.paging != null && results.results != null && !results.results.isEmpty()) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mMessageView.setVisibility(View.GONE);
                    mItemsListAdapter.setItemsList(results.results);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    mMessageText.setText(R.string.search_no_resutls);
                    mMessageView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ItemSearchResults> call, Throwable t) {
                Log.e(TAG, "Error trying to get the items", t);
                mRecyclerView.setVisibility(View.GONE);
                mMessageText.setText(R.string.search_no_resutls);
                mMessageView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);

        final MenuItem search = menu.findItem(R.id.action_search);

        final SearchView searchView = (SearchView) search.getActionView();
        searchView.setQueryHint(getString(R.string.search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.isEmpty()) {
                    requestItems(query);
                    searchView.clearFocus();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }
}
