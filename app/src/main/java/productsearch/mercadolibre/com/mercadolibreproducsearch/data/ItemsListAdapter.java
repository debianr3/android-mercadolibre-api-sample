package productsearch.mercadolibre.com.mercadolibreproducsearch.data;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import productsearch.mercadolibre.com.mercadolibreproducsearch.R;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.Item;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.Reviews;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.Shipping;
import productsearch.mercadolibre.com.mercadolibreproducsearch.ui.ItemDetailsActivity;

public class ItemsListAdapter extends RecyclerView.Adapter<ItemsListAdapter.ViewHolder> {
    private List<Item> mItemList = new ArrayList<>();

    public void setItemsList(@NonNull List<Item> itemList) {
        mItemList = itemList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.set(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitle;
        private TextView mPrice;
        private ImageView mIcon;
        private RatingBar mRatingBar;
        private View mShippingLayout;
        private Item mItem;

        public ViewHolder(final View itemView) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.itemTitle);
            mPrice = itemView.findViewById(R.id.itemPrice);
            mIcon = itemView.findViewById(R.id.itemIcon);
            mRatingBar = itemView.findViewById(R.id.itemRatingBar);
            mShippingLayout = itemView.findViewById(R.id.itemShipping);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent intent = new Intent(itemView.getContext(),
                                                     ItemDetailsActivity.class);

                    intent.putExtra(ItemDetailsActivity.EXTRA_ITEM, mItem);

                    itemView.getContext().startActivity(intent);
                }
            });
        }

        public void set(final Item item) {
            mItem = item;
            mTitle.setText(item.getTitle());
            // TODO: Get currency dinamically.
            mPrice.setText("$" + item.getPrice());

            final Reviews reviews = item.getReviews();
            if (reviews != null && reviews.getRatingAverage() > 0) {
                mRatingBar.setVisibility(View.VISIBLE);
                mRatingBar.setRating((float) reviews.getRatingAverage());
            } else {
                mRatingBar.setVisibility(View.INVISIBLE);
            }

            final Shipping shipping = item.getShipping();
            if (shipping != null && shipping.getFreeShipping()) {
                mShippingLayout.setVisibility(View.VISIBLE);
            } else {
                mShippingLayout.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(item.getThumbnail())) {
                Picasso.get().load(item.getThumbnail()).fit().centerCrop().into(mIcon);
            }
        }
    }
}
