package productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemSearchResults {

    @SerializedName("site_id")
    @Expose
    public String siteId;
    @SerializedName("query")
    @Expose
    public String query;
    @SerializedName("paging")
    @Expose
    public Paging paging;
    @SerializedName("results")
    @Expose
    public List<Item> results = null;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ItemSearchResults{");
        sb.append("siteId='").append(siteId).append('\'');
        sb.append(", query='").append(query).append('\'');
        sb.append(", paging=").append(paging);
        sb.append(", items=").append(results);
        sb.append('}');
        return sb.toString();
    }
}
