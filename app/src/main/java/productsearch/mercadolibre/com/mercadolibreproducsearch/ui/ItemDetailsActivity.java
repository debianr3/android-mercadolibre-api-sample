package productsearch.mercadolibre.com.mercadolibreproducsearch.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import productsearch.mercadolibre.com.mercadolibreproducsearch.R;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.ApiClient;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.Item;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.Reviews;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.Shipping;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.detail.ItemDetails;
import productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item.detail.Picture;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_ITEM = "key_item";
    private static final String TAG = ItemDetailsActivity.class.getSimpleName();

    private ImagesPagerAdapter mImagesPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        final Item item = getIntent().getParcelableExtra(EXTRA_ITEM);
        if (item == null) {
            Log.e(TAG, "Error: Item not set");
            finish();
            return;
        }

        final TextView title = findViewById(R.id.itemDetailsTitle);
        title.setText(item.getTitle());

        final TextView price = findViewById(R.id.itemDetailsPrice);
        // TODO: Get currency dinamically.
        price.setText("$" + item.getPrice());

        final View ratingBarLayout = findViewById(R.id.itemDetailsRatingBarLayout);
        final Reviews reviews = item.getReviews();
        if (reviews != null && reviews.getRatingAverage() > 0) {
            final RatingBar ratingBar = findViewById(R.id.itemDetailsRatingBar);
            final TextView ratingCount = findViewById(R.id.itemDetailsRatingCount);
            ratingBar.setRating((float) reviews.getRatingAverage());

            final int total = reviews.getTotal();
            ratingCount.setText(
                    getResources().getQuantityString(R.plurals.ratingCount, total, total));
        } else {
            ratingBarLayout.setVisibility(View.GONE);
        }

        final ViewPager viewPager = findViewById(R.id.itemDetailsViewPager);
        mImagesPagerAdapter = new ImagesPagerAdapter();
        viewPager.setAdapter(mImagesPagerAdapter);

        final View shippingLayout = findViewById(R.id.itemDetailShipping);
        final Shipping shipping = item.getShipping();
        if (shipping != null && shipping.getFreeShipping()) {
            shippingLayout.setVisibility(View.VISIBLE);
        } else {
            shippingLayout.setVisibility(View.GONE);
        }


        final TextView picturesCount = findViewById(R.id.itemDetailsPicturesCount);

        requestDetails(item.getId(), mImagesPagerAdapter, picturesCount);
    }

    private void requestDetails(final String itemId, final ImagesPagerAdapter imagesPagerAdapter, final TextView picturesCount) {
        final Call<ItemDetails> detailsRequest =
                ApiClient.INSTANCE.getService().getItemDetails(itemId);

        detailsRequest.enqueue(new Callback<ItemDetails>() {
            @Override
            public void onResponse(Call<ItemDetails> call, Response<ItemDetails> response) {
                final ItemDetails itemDetails = response.body();
                if (itemDetails != null) {
                    List<Picture> pictures = itemDetails.getPictures();
                    if (pictures == null) {
                        pictures = new ArrayList<>();
                    } else if (!pictures.isEmpty()) {
                        picturesCount.setVisibility(View.VISIBLE);
                        picturesCount.setText(
                                getResources().getQuantityString(R.plurals.picturesCount,
                                                                 pictures.size(), pictures.size()));
                    }

                    imagesPagerAdapter.setPictures(pictures);
                }
            }

            @Override
            public void onFailure(Call<ItemDetails> call, Throwable t) {
                Log.e(TAG, "Error trying to get the item details", t);
            }
        });
    }

    private static class ImagesPagerAdapter extends PagerAdapter {

        private List<Picture> mPictures = new ArrayList<>();

        public void setPictures(final List<Picture> pictures) {
            this.mPictures = pictures;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mPictures.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            final ImageView imageView = new ImageView(container.getContext());
            final Picture picture = mPictures.get(position);
            if (!TextUtils.isEmpty(picture.getUrl())) {
                Picasso.get().load(picture.getUrl()).fit().centerCrop().into(imageView);
            }
            container.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }
    }
}
