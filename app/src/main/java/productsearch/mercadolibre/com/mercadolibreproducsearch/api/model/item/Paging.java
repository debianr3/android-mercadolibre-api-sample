package productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Paging implements Parcelable {

    @SerializedName("total")
    @Expose
    public Integer total;
    @SerializedName("offset")
    @Expose
    public Integer offset;
    @SerializedName("limit")
    @Expose
    public Integer limit;
    @SerializedName("primary_results")
    @Expose
    public Integer primaryResults;
    public final static Parcelable.Creator<Paging> CREATOR = new Creator<Paging>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Paging createFromParcel(Parcel in) {
            return new Paging(in);
        }

        public Paging[] newArray(int size) {
            return (new Paging[size]);
        }

    };

    protected Paging(Parcel in) {
        this.total = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.offset = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.limit = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.primaryResults = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public Paging() {
    }

    public Paging withTotal(Integer total) {
        this.total = total;
        return this;
    }

    public Paging withOffset(Integer offset) {
        this.offset = offset;
        return this;
    }

    public Paging withLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public Paging withPrimaryResults(Integer primaryResults) {
        this.primaryResults = primaryResults;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(total);
        dest.writeValue(offset);
        dest.writeValue(limit);
        dest.writeValue(primaryResults);
    }

    public int describeContents() {
        return 0;
    }
}
