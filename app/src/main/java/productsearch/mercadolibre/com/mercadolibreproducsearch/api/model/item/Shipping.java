
package productsearch.mercadolibre.com.mercadolibreproducsearch.api.model.item;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class Shipping implements Parcelable {

    @SerializedName("free_shipping")
    @Expose
    private boolean freeShipping;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("tags")
    @Expose
    private List<Object> tags = null;
    @SerializedName("logistic_type")
    @Expose
    private String logisticType;
    public final static Creator<Shipping> CREATOR = new Creator<Shipping>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Shipping createFromParcel(Parcel in) {
            return new Shipping(in);
        }

        public Shipping[] newArray(int size) {
            return (new Shipping[size]);
        }

    };

    protected Shipping(Parcel in) {
        this.freeShipping = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.mode = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.tags, (Object.class.getClassLoader()));
        this.logisticType = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Shipping() {
    }

    public boolean getFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    public String getLogisticType() {
        return logisticType;
    }

    public void setLogisticType(String logisticType) {
        this.logisticType = logisticType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Shipping{");
        sb.append("freeShipping=").append(freeShipping);
        sb.append(", mode='").append(mode).append('\'');
        sb.append(", tags=").append(tags);
        sb.append(", logisticType='").append(logisticType).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shipping)) return false;
        Shipping shipping = (Shipping) o;
        return Objects.equals(freeShipping, shipping.freeShipping) &&
                Objects.equals(mode, shipping.mode) &&
                Objects.equals(tags, shipping.tags) &&
                Objects.equals(logisticType, shipping.logisticType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(freeShipping, mode, tags, logisticType);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(freeShipping);
        dest.writeValue(mode);
        dest.writeList(tags);
        dest.writeValue(logisticType);
    }

    public int describeContents() {
        return 0;
    }

}
